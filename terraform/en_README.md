# Set up cluster with Terraform

First of all, we need to create a cluster. We will use Terraform to do it.

First, init Terraform stat
```bash
terraform init
```

then set up some env. variables to access OVH environment
```bash
export SERVICE_NAME=""
export APPLICATION_KEY=""
export APPLICATION_SECRET=""
export CONSUMER_KEY=""
export OVH_CLOUD_PROJECT_KUBE_NAME=""
```
We can also use terraform.tfvars file to populate terraform variables

Now we are ready to check Terraform plan

```bash
terraform plan -out my.plan
```

and launch it !

```bash
terraform apply my.plan
```
During this time go grab a ☕ 

When `apply` is done, we can retrieve our kubeconfig file to be able to connect to the cluster

```bash
source terraform.tfvars
terraform output -raw kubeconfig > cluster-ovh-${OVH_CLOUD_PROJECT_KUBE_NAME}.yml
export KUBECONFIG=./cluster-ovh-${OVH_CLOUD_PROJECT_KUBE_NAME}.yml
[ -d ../../../.kube/custom-contexts ] && cp "./cluster-ovh-${OVH_CLOUD_PROJECT_KUBE_NAME}.yml" ../../../.kube/custom-contexts/
```


```bash
kubectl get nodes  -o wide
kubectl get ns
```

Now, let's deploy something, a simple `Deployment`

```bash
kubectl apply -f ../demos/simple-deployment.yml
```

```bash
kubectl get deployments -n demos
```
And try to access it :

```bash
echo http://localhost:8080
kubectl -n demos port-forward $(kubectl get pods -o=name -n demos) 8080:80
```

and visit [http://localhost:8080](http://localhost:8080)  🎆

BUT: 
 * we can't access it from outside
 * you must map pod port to host port


If you want to access it from outside, you need to create a `Service` and an `Ingress` and add an ingress controller.
See you in the [next chapter](../nginx-ingress-controller/README.md) !




To delete this cluster use 
```bash
terraform destroy 
```

